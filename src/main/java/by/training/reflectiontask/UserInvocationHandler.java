package by.training.reflectiontask;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Class using as handler for proxy factory.
 */
public class UserInvocationHandler implements InvocationHandler {

    final static Logger logger = Logger.getLogger(UserInvocationHandler.class);
    private Object obj;

    public void setObj(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        logger.info("Proxy object run method : " + method.getName());
        return method.invoke(obj, args);
    }
}
