package by.training.reflectiontask;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for mark object's fields to test for equality.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Equal {
    /**
     * Attribute may be 'value' for check by equals
     * and 'reference' for check by == .
     * @return
     */
    String compareby() default "value";
}
