package by.training.reflectiontask;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

/**
 * Test objects for equality.
 */
class Analyzer {
    final static Logger logger = Logger.getLogger(Analyzer.class);

    /**
     * Return true if fields marked as @Equal annotation equals by reference or by value.
     *
     * @param obj1
     * @param obj2
     * @return
     */
    boolean equalObjects(final Object obj1, final Object obj2) {
        if (obj1 == obj2) {
            return true;
        }

        if (obj1.getClass().equals(obj2.getClass())) {
            Field[] field = obj1.getClass().getDeclaredFields();
            for (Field f : field
                    ) {
                if (f.isAnnotationPresent(Equal.class)) {
                    f.setAccessible(true);
                    try {
                        Field field2 = obj2.getClass().getDeclaredField(f.getName());
                        field2.setAccessible(true);

                        Equal equal = f.getAnnotation(Equal.class);

                        if (f.get(obj1) == null && field2.get(obj2) == null) {
                            continue;
                        }

                        if (equal.compareby().equals("value")) {
                            if (f.get(obj1) != null && f.get(obj1).equals(field2.get(obj2))) {
                                continue;
                            }

                        } else { // check by reference
                            if (f.get(obj1) != null && (f.get(obj1) == field2.get(obj2))) {
                                continue;
                            }
                        }

                        return false;

                    } catch (IllegalAccessException | NoSuchFieldException e) {
                        logger.error(e);
                    }

                }
            }

            return true;
        } else {
            return false;
        }
    }
}
