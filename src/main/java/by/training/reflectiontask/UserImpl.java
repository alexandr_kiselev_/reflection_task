package by.training.reflectiontask;

/**
 * Class contain information about some user.
 */
@Proxy(invocationHandler = "by.training.reflectiontask.UserInvocationHandler")
public class UserImpl implements User {
    @Equal(compareby = "reference")
    private String name;
    @Equal(compareby = "value")
    int age;
    @Equal(compareby = "reference")
    String nickname;
    private String hobby;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) { this.age = age; }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(final String hobby) {
        this.hobby = hobby;
    }

    @Override
    public String printInfo() {
        return "Name: " + getName() + ", age: " + getAge() + ", nickname: "
                + nickname + ", hobby: " + getHobby();
    }
}