package by.training.reflectiontask;

/**
 * Print full information about user.
 */
public interface User {
    String printInfo();
}
