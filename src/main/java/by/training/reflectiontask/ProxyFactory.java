package by.training.reflectiontask;

import org.apache.log4j.Logger;

/**
 * Proxy factory return proxying object if object class contains @Proxy annotation.
 */
class ProxyFactory {

    final static Logger logger = Logger.getLogger(ProxyFactory.class);

    static public <T> T getInstanceOf(final UserImpl obj) {
        if (obj.getClass().isAnnotationPresent(Proxy.class)) {
            Proxy proxyText = obj.getClass().getAnnotation(Proxy.class);
            try {
                Class c = Class.forName(proxyText.invocationHandler());
                Object objForHandler = c.newInstance();
                UserInvocationHandler handler = (UserInvocationHandler) objForHandler;

                handler.setObj(obj);

                return (T) java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(),
                        obj.getClass().getInterfaces(), handler);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                logger.error(e);
            }
        }
        return (T) obj;
    }
}
