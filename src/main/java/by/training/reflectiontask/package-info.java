/**
 * Reflection task contains proxy factory and analyzer to test objects for equality.
 *
 * @author Alexandr_Kiselev
 * @version 1.0
 * @since 1.0
 */
package by.training.reflectiontask;