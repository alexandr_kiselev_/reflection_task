package by.training.reflectiontask;

import org.apache.log4j.Logger;

public class Runner {

    final static Logger logger = Logger.getLogger(Runner.class);

    public static void main(String args[]) {
        String equalText = "Objects are equal : ";
        UserImpl user = new UserImpl();
        UserImpl user2 = new UserImpl();
        user.setAge(18);
        user2.setAge(19);
        String name = "Jonny";
        user.setName(name);
        user2.setName(name);
        user.nickname = new String("JLittle");
        user2.nickname = new String("JLittle");
        user.setHobby("Music");
        user2.setHobby("Football");

        Analyzer analyzer = new Analyzer();
        logger.info(equalText + analyzer.equalObjects(user, user2));
        user2.setAge(18);
        logger.info(equalText + analyzer.equalObjects(user, user2));
        user.nickname = user.nickname.intern();
        user2.nickname = user2.nickname.intern();
        logger.info(equalText + analyzer.equalObjects(user, user2));
        User userProxy = (User) ProxyFactory.getInstanceOf(user);
        logger.info(userProxy.printInfo());
    }
}
